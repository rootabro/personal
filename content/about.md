---
title: "All About Me"
date: 2021-11-19T17:41:52+06:00
draft: false
url: "/about/"
category: Personal
searchHidden: true
ShowReadingTime: true
ShowBreadCrumbs: true
ShowPostNavLinks: true
---

You've landed here to learn about me? Really? Alright. The thing is, I've brought you here - just to let you know that I've been testing this site for quite some time now. Thus, you can't actually extract anything substantial. Funnily, even the text you're reading right now is just a part of a mere test.

## A little bit then?
Yes. I'll tell you just a `little bit`. 

|Items| Details|
|:---|:----|
|**Name**| Mostakim Billah|
|**Location**| Rajshahi University|
|**Country**| Bangladesh|
|**That's it?**| Yes|

----

See you again mate...GOODBYE
