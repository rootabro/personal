---
title: "Keep Copying Till The Very Moment You Make Your Own"
date: 2021-11-19T17:41:52+06:00
draft: false
category: Personal
---
## Sample Text One

Given the three features of (1) setting a field, (2) letting depth
subtyping change the type of a field, and (3) having a sound type system actually prevent field-missing errors, we can have any two of the three, but not all of them.

## Sample Text Two

* Width subtyping: A supertype can have a subset of fields with the same types, i.e., a subtype can do extra.
* Permutation subtypings: A supertype can have the same set of fields with the same types in a different order.
* Transitivity: if t1 is subtype of t2, and t2 is subtype of t3, then t1 is subtype of t3.
* Reflexivity: Every type is a subtype of itself.

## Sample Text Three

Function subtyping **contravariant** in argument(s) and **covariant** in results.
If t3 is subtype of t1, and t2 is a subtype of t4, then `t1 -> t2` is a subtype of `t3 -> t4`.

